"""
BATMAN package
**************
"""
import openturns as ot
ot.RandomGenerator.SetSeed(123456)

__version__ = '1.9'
__branch__ = 'heads/develop'
__commit__ = '1.9-Pennyworth-30-g38d4a6c'
